package com.shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class PaymentServlet
 */
@WebServlet("/PaymentServlet")
public class PaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out =	response.getWriter();
		
		response.setContentType("text/html");
		
				out.print("<h1>Welcome to Payment Gateway</h1>");
				
				
				 // String cname = request.getParameter("cname"); String pname =
				 // request.getParameter("pname"); 
				//int price =
				 // Integer.parseInt(request.getParameter("price"));
				  
				 // Integer amount = (Integer)	request.getAttribute("amount");
				  
				HttpSession session = request.getSession();
				
				Integer amount = (Integer)	session.getAttribute("amount");
					
				
				 // out.print("Dear Customer "+cname+" You are paying for "+pname);
				  out.print("Total Amount Rs."+amount+" is paid successfully");
				 
				  //	session.removeAttribute("amount"); // data will be remove 
				  
				  // logout
				  
				  //	session.invalidate();  //end the session or session object will be deleted
	
				  
			ServletContext  application =	  	request.getServletContext();
				  
			Object obj  =	  	application.getAttribute("list");
			
					List<String> list = (List<String>) obj;
					
					out.println(list);
					
					String cname =	request.getParameter("cname");
					String  pname = request.getParameter("pname");
					  int price = Integer.parseInt(request.getParameter("price"));
					  
					  
					  out.print("all values from hidden fields");
					  
					  out.print(cname +" "+pname+" "+price);
					  
					  
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
