package com.shop;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class CustomerServlet
 */
@WebServlet("/CustomerServlet")
public class CustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

				
			PrintWriter out =	response.getWriter();
			
					response.setContentType("text/html");
			
			out.print("<h2>Welcome to Customer Servlet</h2>");
		
			String cname =	request.getParameter("cname");
			String  pname = request.getParameter("pname");
			  int price = Integer.parseInt(request.getParameter("price"));
			
					out.print("Thank you "+cname+" for purchasing "+pname);
					out.print("<br>  Your Total Bill Amount is Rs. "+price);
					out.print("Your redirecting to payment page....");
					
					out.print("hidden fields available here but you can see them");
					
					out.print("<input type='hidden' name='cname'  value="+cname+">");
					out.print("<input type='hidden' name='pname'  value="+pname+">");
					out.print("<input type='hidden' name='price'  value="+price+">");
			
			  //CustomerServlet sends request to PaymentServlet
		
					//here total amount is calculated which need to pay in payment servlet
					//request.setAttribute("amount", price +1000);
					

					HttpSession session = request.getSession();
					
						session.setAttribute("amount", price + 5000);
						
						
			ServletContext application = request.getServletContext(); // context obj is also known as application object
						
					List<String> list = new ArrayList<String>();
							
					list.add("laptop");  list.add("mobile"); list.add("bike");
			
				application.setAttribute("list", list);
					
					RequestDispatcher reqDispatcher = request.getRequestDispatcher("/PaymentServlet");
					
				reqDispatcher.forward(request, response);
		
					//reqDispatcher.include(request, response);
					
					
					// any java object can be set to scope variable in request,session,application 
					
					
					//response.sendRedirect("PaymentServlet");
					
					
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
