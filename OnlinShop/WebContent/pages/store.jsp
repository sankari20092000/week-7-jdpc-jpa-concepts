<%@ page import="com.shop.*,java.util.*" isELIgnored="false"   
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

this is store page...

<%
	String pname = request.getParameter("pname");

	int price = Integer.parseInt(request.getParameter("price"));

	//String dop = request.getParameter("dop");

	Product product = new Product();

	product.setPname(pname);
	product.setPrice(price);
	product.setDop(new Date());

	session.setAttribute("product", product);

	RequestDispatcher rd = request.getRequestDispatcher("display.jsp");

	rd.forward(request, response);
%>

<%-- 
 <%= request.getParameter("price") %>
 
 JpsExp :   <%= session.getAttribute("dop") %>
 
  EL Exp:      ${dop}        <!--  EL-Exp -->
  
   --%>