package com.shop;

import java.util.Date;

public class Product {
	
	
	private String pname;
	
	private  int price;
	
	private  Date dop;

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
		
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getDop() {
		return dop;
	}

	public void setDop(Date dop) {
		this.dop = dop;
		
		
		
		
	}

	@Override
	public String toString() {
		return "Product [pname=" + pname + ", price=" + price + ", dop=" + dop + "]";
	}
	
	

}
