package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloServlet
 */
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public HelloServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {


		System.out.println("init() executed...");
		
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {


		System.out.println("destroy() executed...");
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	
	// accept get as well as post request
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("service() executed...");
		
		
				PrintWriter out =	response.getWriter();
		
		
				// sending data to the client using response object
					out.print("<h1>Welcome  to Servlet</h1>");
					
					// fetch data from request object browser to server
				String uname =	request.getParameter("uname");
				
				String password =	request.getParameter("pwd");
				
				System.out.println(uname +" "+password);
				
				out.print("Welcome "+uname);
		
		
	}

}
