package com.hcl.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DemoServlet
 */
@WebServlet(name = "ds", urlPatterns = { "/ds" })
public class DemoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DemoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("get executed..");
			
			PrintWriter out =	response.getWriter();
		
				out.print("<h1 style='background-color:red'>Welcome to My Servlet<h1>");
		
			String username1 =	request.getParameter("username1");
			
				String username2 =	request.getParameter("username2");
				
			out.print("Thank you only from get "+username1);
			
			out.print("<br>");
			
			out.print("Thank you through post from get "+username2);
			out.print("<br>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


			System.out.println("post executed...");
		
		doGet(request, response);
		
		PrintWriter out =	response.getWriter();
		
		String username2 = request.getParameter("username2");
		
		
			out.print("Thank you from post "+username2);
			out.print("<br>");
	}

}
