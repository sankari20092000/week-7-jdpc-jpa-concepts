package com.hcl.ems.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hcl.ems.dao.EmployeeDAO;
import com.hcl.ems.pojo.Employee;

/**
 * Servlet implementation class EmployeeServlet
 */
@WebServlet("/EmployeeServlet")
public class EmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EmployeeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EmployeeDAO dao = new EmployeeDAO();

		PrintWriter out = response.getWriter();

		out.print("Welcome to Employee servlet");

		String key = request.getParameter("key");

		switch (key) {
		case "INSERT":

			int eid = Integer.parseInt(request.getParameter("eid"));

			String ename = request.getParameter("ename");

			double salary = Double.parseDouble(request.getParameter("salary"));

			Employee employee = new Employee();

			employee.setEid(eid);
			employee.setEname(ename);
			employee.setSalary(salary);

			int count = dao.saveEmployee(employee);

			if (count > 0) {

				out.print("<h1> Recored Successfully Added to DB</h1>");
			} else {

				out.print("<h2> Record Not Added  </h2>");

			}

			break;

		case "UPDATE":

			int eid1 = Integer.parseInt(request.getParameter("eid"));

			String ename1 = request.getParameter("ename");

			double salary1 = Double.parseDouble(request.getParameter("salary"));

			Employee updateEmpObj = new Employee();

			updateEmpObj.setEid(eid1);
			updateEmpObj.setEname(ename1);
			updateEmpObj.setSalary(salary1);

			int updateCount = dao.updateEmployee(updateEmpObj);

			if (updateCount > 0) {

				out.print("<h1> Recored Successfully UPDATED to DB</h1>");
			} else {

				out.print("<h2> Record Not UPDATED  </h2>");

			}

			break;

		case "SELECT":
			
			

			int empID = Integer.parseInt(request.getParameter("eid"));
			
			
				Employee  e1 =	dao.selectById(empID);
				
				out.print("<table border='1'><tr><td>"+e1.getEid()+"</td><td>"+e1.getEname() +"</td><td>"+e1.getSalary()+"</td><td>"+e1.getDoj()+"</td></tr></table>");
				
				
			

			break;

		case "DELETE":
			

			int id = Integer.parseInt(request.getParameter("eid"));
				
			int deleteCount =	dao.deleteEmployeeById(id);
			
			
			if (deleteCount > 0) {

				out.print("<h1> Recored Successfully DELETED FROM DB</h1>");
			} else {

				out.print("<h2> Record Not deleted sorry!  </h2>");

			}
			
			
				break;
				

		
		case "SELECT_ALL" :
			
			
			List<Employee> empList = dao.getAllEmployees();
			
			out.print("<table border='1'>");
				
			for (Employee emp : empList) {
				
				
				out.print("<tr><td>"+emp.getEid()+"</td><td>"+emp.getEname() +"</td><td>"+emp.getSalary()+"</td><td>"+emp.getDoj()+"</td></tr>");
				
			}
			
			
			out.print("</table>");
			
			
			break;
				

		default:
			break;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
