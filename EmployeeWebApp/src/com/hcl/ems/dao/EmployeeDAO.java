package com.hcl.ems.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.ems.pojo.Employee;

//create table employee(eid integer,ename varchar(20),salary double(10,2),doj date); 
//use above query to create table in mysql Database
// please ensure your mysql username , password , database name is correct in DBUtil file


public class EmployeeDAO {

	Connection conn = DBUtil.getDBConnection();

	int insertCount = 0;

	public int saveEmployee(Employee emp) {

		String query = "insert into employee(eid,ename,salary,doj) values(?,?,?,current_date)";
		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, emp.getEid());
			pstmt.setString(2, emp.getEname());
			pstmt.setDouble(3, emp.getSalary());

			insertCount = pstmt.executeUpdate();

			System.out.println(insertCount + " recored inserted...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return insertCount;

	}

	public int updateEmployee(Employee emp) {

		int updateCount = 0;

		String updateQuery = "update employee set ename =? , salary =? , doj = current_date+1  where eid =?";

		PreparedStatement pstmt;

		try {
			pstmt = conn.prepareStatement(updateQuery);

			pstmt.setString(1, emp.getEname());
			pstmt.setDouble(2, emp.getSalary());
			pstmt.setInt(3, emp.getEid());

			updateCount = pstmt.executeUpdate();

			System.out.println(updateCount + " recored updated...");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return updateCount;

	}

	public Employee selectById(int eid) {

		Employee employee = null; // this emp object is used to return the single record from dao to servlet

		String query = "select * from employee where eid =?";

		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(query);

			pstmt.setInt(1, eid); // select record for given eid

			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {

				int eid1 = rs.getInt(1);

				String ename1 = rs.getString("ename");

				double salary1 = rs.getDouble("salary");

				Date doj = rs.getDate("doj");

				System.out.println(eid1 + "  " + ename1 + " " + salary1 + " " + doj);

				employee = new Employee(); // data got from DB and store in emp object and returned
				employee.setEid(eid1);
				employee.setEname(ename1);
				employee.setSalary(salary1);
				employee.setDoj(doj);								// to servlet
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return employee;

	}

	public int deleteEmployeeById(int eid) {

		int deleteCount = 0;

		try {
			String deleteQuery = "delete from employee where eid =?";
			PreparedStatement pstmt = conn.prepareStatement(deleteQuery);

			pstmt.setInt(1, eid);

			deleteCount = pstmt.executeUpdate();

			System.out.println(deleteCount + " record deleted... ");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return deleteCount;
	}

	public List<Employee> getAllEmployees() { // no input just select all

		List<Employee> empList = new ArrayList<Employee>();
		// here List object will help us to return all employees records to the servlet
		// in collection format

		String query = "select * from employee";

		PreparedStatement pstmt;
		try {
			pstmt = conn.prepareStatement(query);

			ResultSet rs = pstmt.executeQuery(); // select all records

			while (rs.next()) {

				int eid1 = rs.getInt(1);

				String ename1 = rs.getString("ename");

				double salary1 = rs.getDouble("salary");

				Date doj = rs.getDate("doj");

				System.out.println(eid1 + "  " + ename1 + " " + salary1 + " " + doj);

				Employee emp = new Employee();

				emp.setEid(eid1);
				emp.setEname(ename1);
				emp.setSalary(salary1);
				emp.setDoj(doj);

				// here emp obje is created and data from ResultSet is fetch
				// if in DB 10 records are there then 10 Employee object will be created in
				// while loop
				// all these employee objects will be added to list one by one and return to
				// servlet

				empList.add(emp); // adding employee object one by one to list collection

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return empList;

	}

}
