package com.hcl.ems.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
	
	
	public static  Connection   getDBConnection() {
		
		Connection conn= null;
		
		try {
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			
			// step 2 get the DB connection

			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcldb", "root", "admin");
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return conn;
		
		
	}

}
