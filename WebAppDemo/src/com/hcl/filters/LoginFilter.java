package com.hcl.filters;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(urlPatterns ="/filter" , servletNames = { "HomeServlet" } )
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain
		
	PrintWriter out =	response.getWriter();
		
				System.out.println("Filter executed...");
		
//			HttpServletRequest httpRequest = (HttpServletRequest) request;
//			HttpServletResponse httpResponse = (HttpServletResponse) response;
	
				
		if(request.getParameter("uname").equals("admin") && request.getParameter("password").equals("tiger")) {
		
			System.out.println("valid user");
			
			
			
			
			RequestDispatcher rd1 =	request.getRequestDispatcher("/HomeServlet");
			
			rd1.forward(request, response);
		}
		
		else {
			System.out.println("invalid user");
			
				out.print("<h3 style='color:red'>Invalid credentials </h3>");
			
		RequestDispatcher rd2 =	request.getRequestDispatcher("/login.jsp");
			
				rd2.include(request, response);
		
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
